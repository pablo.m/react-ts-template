# React, TypeScript y Webpack template

El objetivo de este proyecto es tener una plantilla para todo proyecto que requiera React, Typescript y Webpack

## Tecnologías elegidas
El front-end es desarrollado con `react@17`, `webpack@5` y `typescript@4`. La plantilla está lista para `styled-components@5`, `Sass` o `PostCSS` para los estilos de los componentes.

## Uso y desarrollo
Para poder probar el código localmente, es necesario contar con una versión de `node@14+` LTS > y `yarn`.

Primero, es necesario instalar todos los paquetes con el siguiente comando:
```
yarn install
```

Después de la instalación, el servidor local se puede ejecutar con el comando:
```
yarn start
```

## Construcción y despliegue
Este trabajo fue desarrollado considerando su uso como una web app. Sin embargo, si se desea hacer un paquete de JavaScript con él, se deberá modificar la configuración de webpack `webpack.config.ts`.

Una vez modificado (o no) el archivo de `webpack`, se puede construir el proyecto con el siguiente comando:
```
yarn build
```
Este comando transpilará el código de TypeScript a JavaScript, creará un archivo para los polyfills y depositará ambos en `@/build`, donde la `@` es la dirección de la carpeta raíz de este proyecto.
