import path from 'path';
import webpack from 'webpack';
import { merge } from 'webpack-merge'

// Plugins
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import ReactRefreshPlugin from '@pmmmwh/react-refresh-webpack-plugin';

// Common
import { babelOptions, Configuration, webpackCommonConfig, EnvVars } from './webpack.common';

const babelOptionsDev = {
  ...babelOptions,
  plugins: [
    ...babelOptions.plugins,
    'react-refresh/babel'
  ]
};

export const webpackConfig = (env: EnvVars): Configuration => {  
  return merge<Configuration>(webpackCommonConfig, {
    entry: {
      main: path.resolve(__dirname, 'src', 'index.tsx'),
      vendors: ['react-refresh/runtime'],
    },
    mode: 'development',
    devtool: 'cheap-module-source-map',
    target: 'web',
    devServer: {
      static: {
        directory: path.join(__dirname, 'public'),
      },
      compress: true,
      hot: true,
      client: {
        overlay: {
          warnings: false
        },
      },
      open: true,
      port: 'auto'
    },
    optimization: {
      runtimeChunk: 'single',
      nodeEnv: 'development'
    },
    module: {
      rules: [
        {
          test: /\.[jt]sx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptionsDev
            },
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true
              }
            }
          ]
        },
        {
          test: /\.m?js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptionsDev
            }
          ]
        }
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.PRODUCTION': env.production || !env.development,
        'process.env.NAME': JSON.stringify(require('./package.json').name),
        'process.env.VERSION': JSON.stringify(require('./package.json').version),
      }),
      new ForkTsCheckerWebpackPlugin(),
      new ReactRefreshPlugin({
        overlay: {
          sockIntegration: 'wds',
        }
      })
    ]
  })
};

export default webpackConfig;
