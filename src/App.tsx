import React, { FC } from 'react';

const App: FC = () => (
  <div>
    Hola, <strong>mundo</strong>. Este proyecto está en el ambiente{' '}
    <code>{process.env.NODE_ENV}</code>.
  </div>
);

export default App;
