import webpack from 'webpack';
import { merge } from 'webpack-merge';

// Plugins
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';

// Common
import { babelOptions, Configuration, webpackCommonConfig, EnvVars } from './webpack.common';


export const webpackConfig = (env: EnvVars): Configuration => {  
  return merge(webpackCommonConfig, {
    mode: 'production',
    devtool: 'source-map',
    target: 'web',
    optimization: {
      nodeEnv: 'production'
    },
    module: {
      rules: [
        {
          test: /\.[jt]sx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptions
            },
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true
              }
            }
          ]
        },
        {
          test: /\.m?js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptions
            }
          ]
        }
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.PRODUCTION': env.production || !env.development,
        'process.env.NAME': JSON.stringify(require('./package.json').name),
        'process.env.VERSION': JSON.stringify(require('./package.json').version),
      }),
      new ForkTsCheckerWebpackPlugin()
    ]
  });
};

export default webpackConfig;
